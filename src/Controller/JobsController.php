<?php


namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Job;
use App\Entity\User;


class JobsController extends AbstractController
{

    /**
     * @Route ("/jobs", methods={"GET"}, name="getJobs")
     */
    public function getAllJobs(EntityManagerInterface $em)
    {
        $repoJob = $em->getRepository(Job::class);
        $jobs = $repoJob->findAll();
        return $this->render("Job/jobs.html.twig", ["jobsTwig" => $jobs]);
    }




    //MIS TRABAJOS, ACABAR DE ENFOCAR
    /**
     * @Route("/myJobs", methods={"GET"}, name="getMyJobs")
     */
    public function getJobsByUserId(EntityManagerInterface $em)
    {
        $repoUser = $em->getRepository(User::class);
        $user = $repoUser->findAll();

        $repoJob = $em->getRepository(Job::class);
        $jobs = $repoJob->findAll();

        return $this->render("Job/jobs.html.twig", ["jobsTwig" => $jobs, "userTwig" => $user]);
    }


    /**
     * @Route("/jobs/newjob", methods={"GET"})
     */
    public function newjobpage()
    {
        return $this->render("NewJob/newjob.html.twig");
    }


    /**
     * @Route("/jobs/newjob"), methods={"POST"}
     */
    public function addnewjob(EntityManagerInterface $em, Request $req)
    {
        $name = $req->request->get("name");
        $category = $req->request->get("category");
        $price = $req->request->get("price");

        $idUserIntroducido = $req->request->get("userId");
        $repo = $em->getRepository(User::class);
        $userId = $repo->find($idUserIntroducido);

        $newjob = new Job();
        $newjob->setName($name);
        $newjob->setCategory($category);
        $newjob->setPrice($price);
        $newjob->setUserId($userId);

        $em->persist($newjob);
        $em->flush();

        return $this->redirectToRoute("getJobs");
    }

    /**
     * @Route("/jobs/details/{id}", methods={"GET"}, name="jobDetails", requirements={"id"="\d+"})
     */
    public function getJobDetails(Job $job)
    {
        return $this->render("");
    }
}
